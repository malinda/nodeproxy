/**
 * Created by Malinda on 9/28/14.
 */
var httpProxy = require("http-proxy");
var url = require("url");

httpProxy.createServer(function (req, res, proxy) {
    var hostname = req.headers.host.split(":")[0];
    var pathname = url.parse(req.url).pathname;

    var options = { host: hostname };
    if (CheckPath(pathname, "/app01")) {
        options.port = 9121;
        options.path =  SetPath(pathname, "/app01/", "/");
        req.url =  SetPath(pathname, "/app01/", "/");
    } else {
        options.port = 80;
        options.path = SetPath(pathname, "/", "/malindardlgApps/");
        req.url =  SetPath(pathname, "/", "/malindardlgApps/");
    }
    proxy.proxyRequest(req, res, options);
    console.log(req.url);

}).listen(9122);


CheckPath = function (path, start) {
    return !path.indexOf(start);
}

SetPath = function (path, start, newStart) {
    return path.replace(start, newStart);
}

console.log("Proxy listening on port 9122");